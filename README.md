# Les infos des consultants Lydra

## Christophe CHAUDIER

📧 christophe@lydra.fr \
📱 07 68 13 65 12 \
📱 Signal ID : cchaudier.42

### Git

- Froggit : [cchaudier](https://lab.frogg.it/cchaudier)
- GitLab : [cchaudier](https://gitlab.com/cchaudier)
- GitHub : [cchaudier](https://github.com/cchaudier)
- Bitbuket : [cchaudier](https://bitbucket.org/cchaudier)

### Encryption

- [Clef SSH (ED25519)](ssh/id_ed25519_lydra_cchaudier.pub)
- [Clef SSH (RSA)](ssh/ssh/id_rsa_lydra_cchaudier.pub)

### Providers

- Scaleway : christophe@lydra.fr
- OVH : cc171917-ovh
- Gandi : cchaudier

### Others

- Trello : [cchaudier](https://trello.com/cchaudier)

## Thomas MICHEL

📧 thomas@lydra.fr ([GPG](gpg/AD7CE8C99FDFCAED_thomas@lydra.fr.asc)) \
📧 (alternate) thomas.michel@esprit-libre-conseil.com ([GPG](gpg/5C04941D0E1FDCD8_thomas.michel@esprit-libre-conseil.com.asc)) \
📱 07 82 05 32 56 \
📱 Signal ID : tmi.56

### Git

- Froggit : [michel-thomas](https://lab.frogg.it/michel-thomas)
- GitLab : [michel-thomas](https://gitlab.com/michel-thomas)
- GitHub : [michel-thomas](https://github.com/michel-thomas)
- Framagit : [michel-thomas](https://framagit.org/michel-thomas)

### Encryption

- [Clef SSH (ED25519)](ssh/id_ed25519_lydra_tmichel.pub)
- [Clef SSH (RSA)](ssh/id_rsa_lydra_tmichel.pub)
- [Clef GPG (thomas@lydra.fr)](gpg/AD7CE8C99FDFCAED_thomas@lydra.fr.asc)
- [Clef GPG (thomas.michel@esprit-libre-conseil.com)](gpg/5C04941D0E1FDCD8_thomas.michel@esprit-libre-conseil.com.asc)

### Providers

- Scaleway : thomas@lydra.fr
- OVH : mt72020-ovh
- Gandi : espritlibre

### Others

- Trello : [michel_thomas](https://trello.com/u/michel_thomas)

## Arthur BOUDREAULT

📧 arthur@lydra.fr \
📱 Signal ID : artybdrlt.99

### Git

- Froggit : [arthur.boudreault](https://lab.frogg.it/arthur.boudreault)
- GitLab : [artybdrlt](https://gitlab.com/artybdrlt)
- GitHub : [artybdrlt](https://github.com/artybdrlt)

### Encryption

- [Clef SSH (ED25519)](ssh/id_ed25519_lydra_aboudreault.pub)

### Providers

- Scaleway : arthur@lydra.fr
- OVH : ba2420349-ovh

## Daniel GONÇALVES

📧 daniel@lydra.fr \
📧 (alternate) daniel.gonc@lves.fr ([GPG](gpg/00ED4AB2C41B0B7C_daniel.gonc@lves.fr.asc)) \
📱 Signal ID : daniel.972

### Git

- Froggit : [dangoncalves](https://lab.frogg.it/dangoncalves)
- GitLab : [dangoncalves](https://gitlab.com/dangoncalves)
- GitHub : [dangoncalves](https://github.com/dangoncalves)

### Encryption

- [Clef SSH (ED25519)](ssh/id_ed25519_lydra_dgoncalves.pub)
- [Clef GPG (daniel.gonc@lves.fr)](gpg/00ED4AB2C41B0B7C_daniel.gonc@lves.fr.asc)

## Céleste ROBERT

📧 celeste@lydra.fr \
📱 Signal ID : marceline.15

### Git

- Froggit : [Plumtree3D](https://lab.frogg.it/Plumtree3D)
- GitLab : [Plumtree3D](https://gitlab.com/Plumtree3D)
- GitHub : [Plumtree3D](https://github.com/Plumtree3D)

### Encryption

- [Clef SSH (ED25519)](ssh/id_ed25519_lydra_Plumtree3D.pub)

## Sylvain VIART

📧 sylvain@lydra.fr \
📱 Signal ID : Sylvain_agu3l.74

### Git

- Froggit :  [sylvain](https://lab.frogg.it/sylvain)
- GitLab : [Sylvain404](https://gitlab.com/Sylvain404)
- GitHub : [Sylvain303](https://github.com/Sylvain303)

### Encryption

- [Clef SSH (ED25519)](ssh/id_ed25519_lydra_sylvaina.pub)
- [Clef GPG (sylvain@lydra.fr)](gpg/46371E727A7475B7D366CC95ECAC5C7E3118DBFE_sylvain@lydra.fr.asc)
